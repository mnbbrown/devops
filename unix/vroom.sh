#!/bin/bash

clear

echo -n "Please enter a hostname (____.mnbbrown.com): "
read -e HOSTNAME
FQDN="$HOSTNAME.mnbbrown.com"

echo "Installing Updates..."
apt-get update
apt-get -y install aptitude
aptitude -y full-upgrade
echo -e "Complete...\n"

echo "Setting Hostname..."
echo $HOSTNAME > /etc/hostname
hostname -F /etc/hostname
echo -e "Complete...\n"

echo "Configuring Hosts..."	##
IP=`ifconfig eth0 | awk -F: '/inet addr:/ {print $2}' | awk '{ print $1 }'`

echo -e "#IPv4" > /etc/hosts
echo -e "127.0.0.1\tlocalhost" >> /etc/hosts
echo -e "127.0.1.1\tubuntu" >> /etc/hosts
echo -e "$IP\t$HOSTNAME\t$FQDN" >> /etc/hosts

echo -e "\n#IPv6" >> /etc/hosts
echo -e "::1\tip6-localhost ip6-loopback" >> /etc/hosts
echo -e "fe00::0\tip6-localnet" >> /etc/hosts
echo -e "ff00::0\tip6-mcastprefix" >> /etc/hosts
echo -e "ff02::1\tip6-allnodes" >> /etc/hosts
echo -e "ff02::2\tip6-allrouters" >> /etc/hosts
echo "Complete...\n"	##

echo "Configuring Timezones"
ln -sf /usr/share/zoneinfo/Australia/Brisbane /etc/localtime
echo "Complete...\n"

echo "Adding non-root user"
addgroup admin
adduser --disabled-password --gecos "Matthew Brown,,," mnbbrown
password=`echo @aussie39 | mkpasswd -s`
usermod -p $password mnbbrown
adduser mnbbrown admin
echo "Complete...\n"

echo "Securing SSH"
apt-get install htop
mkdir /home/mnbbrown/.ssh
echo 'ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDgHhc5AhE1eSTt+5j183alyw0GVK5/yY8NBmyDFwl26kpGx6mYiXz4wQVuaqpet2LK+Eml8hIzv1kuzOvo+pN1YTPPOTX/6CezVQgVmlIhsiRB7ZfTIRH1IrPUdXp1Cc8pUvSWdrVdVwWEvYKBniDnCCfhuakNTmKx6VLPbkdbNmkRsqVK2e6UVLUY6pWY5tJORflNNE2+GXwpSD6gEkk+fVTIOdZH+b+dLmbOQ2iQT2lHgEduzLP/XkaDGNjg+XPIZw8kcMEO2WSw9STY8XnOcYIPmWzeZs1dDlLm2FEzYNYJeIeDSD3pBB3U9lViMLF1vf43XaohUhuF/mSZVamv mnbbrown@gmail.com' >> /home/mnbbrown/.ssh/authorized_keys
sed -i 's/^PermitRootLogin.*$/PermitRootLogin\tno/g' /etc/ssh/sshd_config
sed -i 's/^\#AuthorizedKeysFile.*$/AuthorizedKeysFile\t\%h\/.ssh\/authorized_keys/g' /etc/ssh/sshd_config
service ssh restart
echo "Complete...\n"

echo "Configuring UFW"
ufw allow ssh
ufw enable
echo "Complete...\n"