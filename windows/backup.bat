@echo off

set backup_server=%1
set backup_dir=%backup_server%\%COMPUTERNAME%
set log_location=%backup_server%\backup-%COMPUTERNAME%.log

NET USE %backup_server% /u:%2 "%3"
md %backup_dir%

date /t >> %log_location%
time /t >> %log_location%
echo user: %username% >> %log_location%

echo Backing Up %USERPROFILE% to %COMPUTERNAME%
robocopy "%userprofile%" "%backup_dir%" /MIR /PURGE /FFT /Z /R:5 /W:10 /MT /XD AppData /XA:SH /XF *.bak *.tmp *.lnk /XJ /LOG+:%log_location% /TEE

NET USE %backup_server% /D